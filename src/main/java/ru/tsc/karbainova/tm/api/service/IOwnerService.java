package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.api.repository.IOwnerRepository;
import ru.tsc.karbainova.tm.model.AbstractOwnerEntity;

import java.util.List;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IOwnerRepository<E> {
}
