package ru.tsc.karbainova.tm.command.auth;

import ru.tsc.karbainova.tm.command.AuthAbstractCommand;
import ru.tsc.karbainova.tm.model.User;

public class AuthViewProfileCommand extends AuthAbstractCommand {
    @Override
    public String name() {
        return "show-profile";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "View Profile";
    }

    @Override
    public void execute() {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("Login: " + user.getLogin());
        System.out.println("Email: " + user.getEmail());
        System.out.println("First name: " + user.getFirstName());
        System.out.println("Last name: " + user.getLastName());
        System.out.println("Middle name: " + user.getMiddleName());
    }
}
